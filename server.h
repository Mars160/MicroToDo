#ifndef SERVER_H
#define SERVER_H
#include <string>
#include <QtCore>
#include <QTcpServer>

class Server: public QTcpServer
{
    Q_OBJECT;
private:
    const QHostAddress address = QHostAddress::Any;
    bool waitFlag = true;
    QTcpSocket* client;
    QString code;

public:
    Server();
    void start(int port);
    void next();
    void read();

    const QString& getCode();

signals:
    void readDone();
};

#endif // SERVER_H
