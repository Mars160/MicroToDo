#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QBuffer>

class HttpRequest :public QNetworkAccessManager
{
    Q_OBJECT
private:
    QNetworkRequest request;
    QNetworkReply* reply;
    QBuffer buffer;
public:
    QNetworkAccessManager* pNaManage;
    void setHeader(QNetworkRequest::KnownHeaders key, QVariant value){request.setHeader(key, value);}
    void setHeader(QString key, QString value){request.setRawHeader(key.toUtf8(), value.toUtf8());}
    QString PostJson(QString url, QJsonObject json, bool wait = true);
    QString Post(QString url,QString str);
    QString Get(QString url);
    QString Delete(QString url);
    void Patch(QString url, QJsonObject json);
    const QNetworkReply* getResponse(){return reply;}
};


#endif // HTTPREQUEST_H
