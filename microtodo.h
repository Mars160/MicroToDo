#ifndef MICROTODO_H
#define MICROTODO_H

#include <QWidget>
#include <QApplication>
#include "server.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include "HttpRequest.h"
#include "QDebug"
#include <QNetworkReply>
#include "settings.h"
#include <QResizeEvent>
#include "ClickableLabel.h"
#include <set>

QT_BEGIN_NAMESPACE
namespace Ui { class MicroToDo; class Settings;}
QT_END_NAMESPACE

class MicroToDo : public QWidget
{
    Q_OBJECT

private:
    QMap<QString, ClickableLabel*> compledTask, doingTask;
    std::set<QString> changed;
    QString refresh_token;
    bool typing = true;
    bool showDone = false;//显示已经完成的吗
    bool haveUpdate = false;
    struct task{
        QString id;
        QString importance;
        bool isReminderOn;
        QString status;
        QString title;
    };
    int width, height;
    QString listId;
    QCursor cursor;
    bool moving = false;
    QPoint reltvPos;
    QSqlQuery* query = 0;
    QString selectUrl;
    QSqlDatabase db;
    Server* s = 0;
    QTimer timer, refreshTimer;
    Ui::MicroToDo *ui = 0;
    QApplication* mainApp = 0;
    HttpRequest* req = 0;
    QNetworkReply* reply = 0;
    void showSettings();
    void closeEvent(QCloseEvent *e) override;
    void updateTasks(const task& todo);
    void refreshToken();

public:
    QString token;
    MicroToDo(QApplication* mainApp ,QWidget *parent = nullptr);
    ~MicroToDo();
    void changeColor(unsigned char r, unsigned char g, unsigned char b, unsigned char alpha, bool);
    void setFont(QString fontName, unsigned char r, unsigned char g, unsigned char b, int size_, bool bold, bool italic, bool changeDb);
    Settings* set = 0;

public slots:
    void exit(){mainApp->exit(0);}

private slots:
    void refreshData();
    void getData();

    void on_addLabel_clicked();

    void on_addLine_editingFinished();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void resizeEvent(QResizeEvent* event) override;




};
#endif // MICROTODO_H
