#include "server.h"
#include <QDebug>
#include <QTcpSocket>

Server::Server(){
    connect(this, &Server::newConnection, this, &Server::next);
}

void Server::read(){
    QString result = QString::fromUtf8(client->readAll());
    result = result.mid(result.indexOf("M"));
    QString re = "HTTP/1.1 200 OK\r\nContent-Type: text/html;charset=utf-8\r\nconnection: close\r\n\r\n<html><body>授权完成，现在您可以回到应用了</body></html>";
    client->write(re.toStdString().c_str());
    client->close();
    close();
    //qDebug() << result;
    code = result.mid(0, result.indexOf(" "));

    emit readDone();
}

void Server::next(){
    client = this->nextPendingConnection();
    connect(client, &QTcpSocket::readyRead, this, &Server::read);
}

void Server::start(int Myport){
    listen(address ,Myport);
    //qDebug() << "Listen Start";
}

const QString& Server::getCode(){
    return code;
}
